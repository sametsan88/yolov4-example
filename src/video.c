#include "darknetFO.h"

int main(int argc, char *argv[])
{

    network *ag;

    cap_cv *kamera;
    float esik_degeri = 0.5;
    char isim_listesi_dosyasi[] = "./cfg/coco.names";
    char ayar_dosyasi[] = "./cfg/yolov4.cfg";
    char agirlik_dosyasi[] = "./cfg/yolov4.weights";
    char video_dosyasi[] = "./data/Coffin.mp4";
    nesneler nesnelerim;
    int src_fps = 25;
    int genislik = 608, yukseklik = 608;

    printf("Video açılıyor. \n");
    if (goruntu_ac(&kamera, video_dosyasi) < 0)
    {
        printf("Görüntü açılamadı.");
        exit(-1);
    }

    // Video oluştur
    src_fps = goruntu_fps(kamera);
    printf("Video FPS : %d \n",src_fps);
    printf("sonuc_video oluşturuluyor. \n");
    write_cv *video_writer = create_video_writer("./output/sonuc_video.avi", 'M', 'J', 'P', 'G', src_fps, genislik, yukseklik, 1);

    // İsim listesi yükleniyor
    printf("Nesneler yükleniyor. \n");
    nesneler_yukle(&nesnelerim, isim_listesi_dosyasi);

    // Ağ yükelniyor
    ag_yukle(&ag, ayar_dosyasi, agirlik_dosyasi);
    printf("Ağ Giriş : %d | Çıkış : %d \n", ag->inputs, ag->outputs);

    while (1)
    {
        image goruntu;
        tespit_edilenler tespitlerim;
        mat_cv *goruntu_matris;

        printf("Görüntü geliyor...\n");
        goruntu_al(kamera, &goruntu, &goruntu_matris, genislik, yukseklik);

        if (goruntu.w <= 0 || goruntu.h <= 0)
        {
            printf("Görüntü yok. \n");
            continue;
        }

        // Ağ çalıştırılıyor
        printf("Ağ çalışıyor... %dx%d\n", goruntu.w, goruntu.h);
        ag_calistir(ag, goruntu, esik_degeri, &tespitlerim);

        printf("Tespit edilenler çiziliyor.\n");
        tespit_edilenleri_matrise_ciz(goruntu_matris, tespitlerim, nesnelerim, esik_degeri);

        printf("Video kaydediliyor...\n");
        write_frame_cv(video_writer,goruntu_matris);

        printf("Resim kaydediliyor.\n");
        goruntu = mat_to_image_cv(goruntu_matris);
        resim_kaydet(goruntu,"./output/video");

        // Görüntü sil
        printf("Bellek boşaltılıyor.\n");
        resim_sil(goruntu);
        release_mat(&goruntu_matris);

        // Tespitleri sil
        tespit_edilenler_sil(&tespitlerim);
        printf("Sıradaki..\n");
    }

    release_video_writer(&video_writer);

    ag_sil(ag);

    return 0;
}
