#include "ayarlar.h"

ayar *ayar_satir_oku(FILE *f)
{
    ayar *_ayar = calloc(sizeof(ayar), 1);
    _ayar->degisken = (char *)calloc(sizeof(char), 1);
    _ayar->deger = (char *)calloc(sizeof(char), 1);
    _ayar->degisken_uzunluk = 0;
    _ayar->deger_uzunluk = 0;

    char ch;

    while ((ch = getc(f)) != '=')
    {
        if (ch == '#' || ch == '\n')
        {
            ayar_bosalt(_ayar);
            return NULL;
        }
        _ayar->degisken[_ayar->degisken_uzunluk] = ch;
        _ayar->degisken_uzunluk++;
        _ayar->degisken = (char *)realloc(_ayar->degisken, _ayar->degisken_uzunluk + 1);
    }

    _ayar->degisken[_ayar->degisken_uzunluk] = '\0';

    while ((ch = getc(f)) != '\n' && !feof(f))
    {
        _ayar->deger[_ayar->deger_uzunluk] = ch;
        _ayar->deger_uzunluk++;
        _ayar->deger = (char *)realloc(_ayar->deger, _ayar->deger_uzunluk + 1);
    }
    _ayar->deger[_ayar->deger_uzunluk] = '\0';

    return _ayar;
}

ayarlar *ayar_dosya_oku(char *dosya_adresi)
{
    ayarlar *_ayarlar = (ayarlar *)calloc(sizeof(ayarlar), 1);

    _ayarlar->liste = (ayar **)calloc(sizeof(ayar *), 1);

    FILE *f = fopen(dosya_adresi, "r");

    if (f == NULL)
    {
        printf("Dosya okunamıyor.\n");
        return NULL;
    }

    _ayarlar->adet = 0;
    while (!feof(f))
    {
        ayar *_ayar;
        if ((_ayar = ayar_satir_oku(f)) != NULL)
        {
            _ayarlar->liste = (ayar **)realloc(_ayarlar->liste, sizeof(ayar *) * (_ayarlar->adet + 1));
            _ayarlar->liste[_ayarlar->adet] = (ayar *)calloc(sizeof(ayar), 1);
            _ayarlar->liste[_ayarlar->adet] = _ayar;
            _ayarlar->adet++;
        }
    }

    fclose(f);

    return _ayarlar;
}

char *ayar_al(ayarlar *_ayarlar, const char *degisken)
{

    for (int i = 0; i < _ayarlar->adet; i++)
    {
        if (strcmp(_ayarlar->liste[i]->degisken, degisken) == 0)
        {
            return _ayarlar->liste[i]->deger;
        }
    }
}

int ayar_bosalt(ayar *_ayar)
{
    free(_ayar->degisken);
    free(_ayar->deger);
    free(_ayar);
    return 0;
}

int ayarlar_bosalt(ayarlar *_ayarlar)
{

    for (int i = 0; i < _ayarlar->adet; i++)
        ayar_bosalt(_ayarlar->liste[i]);

    free(_ayarlar->liste);
    free(_ayarlar);
    return 0;
}