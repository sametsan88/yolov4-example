#define DEBUG

#include "darknetFO.h"
#include "ayarlar.h"

int main(int argc, char *argv[])
{

    nesneler nesnelerim;
    tespit_edilenler tespitlerim;
    network *ag;
    image resim;
    image boyutlandirilmis_resim;

    ayarlar* ayarlarim = ayar_dosya_oku(argv[1]);
    float esik_degeri = atof(ayar_al(ayarlarim,"esik_deger"));
    char *isim_listesi_dosyasi = ayar_al(ayarlarim,"isim_listesi_dosyasi");
    char *ayar_dosyasi =  ayar_al(ayarlarim,"ayar_dosyasi");
    char *agirlik_dosyasi =  ayar_al(ayarlarim,"agirlik_dosyasi");
    char *resim_dosyasi =  ayar_al(ayarlarim,"resim_dosyasi");
    char *cikti_dosyasi = ayar_al(ayarlarim,"cikti_dosyasi");

    /*
    float esik_degeri = 0.5;
    char isim_listesi_dosyasi[] = "./cfg/coco.names";
    char ayar_dosyasi[] = "./cfg/yolov4.cfg";
    char agirlik_dosyasi[] = "./cfg/yolov4.weights";
    char resim_dosyasi[] = "./data/horses.jpg";
    char cikti_dosyasi[] = "./output/sonuc.png";
*/


    image **alphabet = load_alphabet();

    // Nesneler yükleniyor
    nesneler_yukle(&nesnelerim, isim_listesi_dosyasi);
    debug("%d adet nesne yüklendi.", nesnelerim.adet);

    // Ağ yükelniyor
    ag_yukle(&ag, ayar_dosyasi, agirlik_dosyasi);
    debug("Ağ giriş : %d | çıkış : %d ", ag->inputs, ag->outputs);

    // Resim yükleniyor
    resim_al(&resim, resim_dosyasi);
    debug("Resim genişlik : %d | yükseklik : %d ", resim.w, resim.h);

    // Resim boyutlandir
    resim_boyutlandir(resim, &boyutlandirilmis_resim, ag->w, ag->h);
    debug("Resim boyutlandırıldı. %dx%d", boyutlandirilmis_resim.w, boyutlandirilmis_resim.h);

    // Ağ çalıştırılıyor
    printf("Ağ çalıştırılıyor...");
    ag_calistir(ag, boyutlandirilmis_resim, esik_degeri, &tespitlerim);
    debug("%d adet nesne tespit edildi.", tespitlerim.nesne_adet);

    // Tespit edilen nesneler
    tespit_edilenleri_resime_ciz(&resim, &tespitlerim, nesnelerim, alphabet,esik_degeri);
    debug("%d adet nesne çizildi.", tespitlerim.nesne_adet);

    // Resim kaydet
    debug("Resim kayediliyor... %s ",cikti_dosyasi);
    resim_kaydet(resim, cikti_dosyasi);

    debug("Bellekler boşaltılıyor... ");
    // Ağ sil
    ag_sil(ag);

    //Resim sil
    resim_sil(resim);

    // Tespitleri sil
    tespit_edilenler_sil(&tespitlerim);

    return 0;
}