#include "darknetFO.h"

image goruntu;
network *ag;
nesneler nesnelerim;
tespit_edilenler tespitlerim;
float esik_degeri = 0.5;
tespit_edilenler tespitler;



void *video_oku_thread(void *arg);

int main(int argc, char *argv[])
{

    char isim_listesi_dosyasi[] = "./cfg/coco.names";
    char ayar_dosyasi[] = "./cfg/yolov4.cfg";
    char agirlik_dosyasi[] = "./cfg/yolov4.weights";
    pthread_t kamera_dongusu;


    // İsim listesi yükleniyor
    nesneler_yukle(&nesnelerim, isim_listesi_dosyasi);

    // Ağ yükelniyor
    ag_yukle(&ag, ayar_dosyasi, agirlik_dosyasi);
    printf("Ağ Giriş : %d | Çıkış : %d \n", ag->inputs, ag->outputs);

    //Video başlatılıyor
    pthread_create(&kamera_dongusu, 0, video_oku_thread, 0);

    while (1)
    {

        if (goruntu.w <= 0 || goruntu.h <= 0)
        {
            printf("Görüntü yok. \n");
            continue;
        }

        debug("Resim alınıyor.");

        image resim_islenecek = copy_image(goruntu);
        if (resim_islenecek.w <= 0 || resim_islenecek.h <= 0)
        {
            free_image(resim_islenecek);
            continue;
        }

        printf("Görüntü geliyor...\n");
        tespit_edilenler_sil(&tespitler);
        // Ağ çalıştırılıyor
        printf("Ağ çalışıyor... %dx%d\n", resim_islenecek.w, resim_islenecek.h);
        ag_calistir(ag, resim_islenecek, esik_degeri, &tespitler);
        printf("Tespit adeti : %d | %d \n", tespitler.tespit_adet, tespitler.nesne_adet);

        tespit_edilenler_sil(&tespitlerim);
        tespit_edilenler_kopyala(tespitler,&tespitlerim);

        printf("Sıradaki..\n");
        resim_sil(resim_islenecek);
    }

    ag_sil(ag);

    pthread_join(kamera_dongusu, NULL);
    pthread_exit(&kamera_dongusu);

    return 0;
}

void *video_oku_thread(void *arg)
{

    cap_cv *kamera;
    kamera_ac(&kamera, 0);
    image **alphabet = load_alphabet();

    while (1)
    {
        // Görüntü al
        resim_sil(goruntu);
        goruntu_al(kamera, &goruntu, NULL, 608, 608);

        tespit_edilenleri_resime_ciz(&goruntu, &tespitlerim, nesnelerim, alphabet, esik_degeri);

        // Görüntüyü göster
        resim_goster(goruntu, "kamera");
    }

    printf("Thread çıkış\n");
}
