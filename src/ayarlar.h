#ifndef AYARLAR_H
#define AYARLAR_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
    char *degisken;
    char *deger;
    int degisken_uzunluk, deger_uzunluk;
} ayar;

typedef struct
{
    ayar **liste;
    int adet;
} ayarlar;

// Ayar dosyası açıp ayarları okur
// dosya_adresi = ayar dosyası adresi
// Geri dönüş ;
//      ayarlar tipinde değişken adresi döner
ayarlar *ayar_dosya_oku(char *dosya_adresi);

// Okunan ayarlar içinde istenileni çekmek için kullanılır
// _ayarlar = ayarlar değişkeni adresi
// degisken = almak istenen ayar ismi
// Geri dönüş ;
//      char* tipinde ayar degeri döner
char *ayar_al(ayarlar *_ayarlar, const char *degisken);

// oluşturulmuş olan ayar değişkenin bellekten silmek için kullanılır
// _ayar = oluşturulmuş ayar değişkeninin adresi
// Geri dönüş ;
//      Başarılı ise 0 döner, başarısız ise -1
int ayar_bosalt(ayar *_ayar);

// oluşturulmuş olan ayarlar değişkenin bellekten silmek için kullanılır
// _ayarlar = oluşturulmuş ayarlar değişkeninin adresi
// Geri dönüş ;
//      Başarılı ise 0 döner, başarısız ise -1
int ayarlar_bosalt(ayarlar *_ayarlar);

#endif