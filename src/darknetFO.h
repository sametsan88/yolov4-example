#ifndef RESIM_H
#define RESIM_H

// debug fonksiyonu tanımlaması
#ifdef DEBUG
#define debug(fmt, args...)   fprintf(stderr, fmt, ## args); printf("\n");
#else
#define debug(fmt, args...)    /* Don't do anything in release builds */
#endif

#include "include/darknet.h"
#include <malloc.h>
#include <pthread.h>

// Darknet kütüphanesinin tanımlanmayan fonksiyonları
typedef void* mat_cv;
char **get_labels_custom(char *filename, int *size);
void draw_bbox(image a, box bbox, int w, float r, float g, float b);
void save_image_png(image p, const char *name);
image load_image_stb(char *filename, int channels);
void draw_label(image a, int r, int c, image label, const float *rgb);
image get_label(image **characters, char *string, int size);
image **load_alphabet();
typedef void *cap_cv;
void show_image_cv(image p, const char *name);
cap_cv *get_capture_webcam(int index);
image get_image_from_stream_cpp(cap_cv *cap);
mat_cv *get_capture_frame_cv(cap_cv *cap);
image mat_to_image_cv(mat_cv *mat);
int wait_key_cv(int delay);
image copy_image(image p);
image get_image_from_stream_resize(cap_cv *cap, int w, int h, int c, mat_cv** in_img, int dont_close);
cap_cv* get_capture_video_stream(const char *path);
typedef void* write_cv;
mat_cv image_to_mat(image img);
write_cv *create_video_writer(char *out_filename, char c1, char c2, char c3, char c4, int fps, int width, int height, int is_color);
void write_frame_cv(write_cv *output_video_writer, mat_cv *mat);
void release_video_writer(write_cv **output_video_writer);
int get_stream_fps_cpp_cv(cap_cv *cap);
image get_image_from_stream_letterbox(cap_cv *cap, int w, int h, int c, mat_cv** in_img, int dont_close);
void draw_detections_cv_v3(mat_cv* mat, detection *dets, int num, float thresh, char **names, image **alphabet, int classes, int ext_output);
void release_mat(mat_cv **mat);


typedef struct
{
    char **liste;
    int adet;
} nesneler;

typedef struct
{
    detection *liste;  // Tespit edilen nesne listesi
    int *nesne_listesi; // Tespit edilen nesnelerin nesne numarası
    int tespit_adet;    // Tespit edilen sayısı
    int nesne_adet;     // Tespit edilen nesne sayısı

} tespit_edilenler;


//  ---------------------------------------------------
//  nesneler değişkenine dosya_adresi'ndeki liste yüklenir
//  nesnelerim  =  nesne listesinin yükleneceği değişkenin adresi
//  dosya_adresi = nesne listesinin dosya adresi
//  Dönen değer;
//      İşlem başarılı ise 0, değil ise -1 döner
//  ----------------------------------------------------
int nesneler_yukle(nesneler *nesnelerim, char *dosya_adresi);

//  ---------------------------------------------------
//  Ağ oluşturma fonksiyonu
//  ag  =  network tipinde bir ağ değişkeni adresi
//  ayar_dosyasi = ayar dosyasinin adresi
//  agirlik_dosyasi = agirlik dosyasinin adresi
//  Dönen değer;
//      İşlem başarılı ise 0, değil ise -1 döner
//  ----------------------------------------------------
int ag_yukle(network **ag, char *ayar_dosyasi, char *agirlik_dosyasi);

//  ---------------------------------------------------
//  Resim dosyasını okuma ve boyutlandırma fonksiyonu
//  resim  =  image tipinde resim değişkeni adresi
//  resim_dosyasi = resim dosyasinin adresi
//  genislik = boyutlandırılacak genişlik
//  yukseklik = boyutlandırılacak yükseklik
//  Dönen değer;
//      İşlem başarılı ise 0, değil ise -1 döner
//  ----------------------------------------------------
int resim_al_boyutlandir(image *resim, char *resim_dosyasi, int genislik, int yukseklik);

//  ---------------------------------------------------
//  Ağ çalıştırma fonksiyonu
//  ag  =  network tipinde bir ağ değişkeni adresi
//  resim = image tipinde resim değişkeni 
//  esik_defer = eşik değeri
//  tespitlerim = tespit edilen nesneler
//  Dönen değer;
//      İşlem başarılı ise 0, değil ise -1 döner
//  ----------------------------------------------------
int ag_calistir(network *ag, image resim, float esik_deger, tespit_edilenler *tespitlerim);

//  ---------------------------------------------------
//  Dosyadan resim okuma fonksiyonu
//  resim = image tipinde resim değişkeni adresi
//  resim_dosyasi = resim dosya adresi
//  Dönen değer;
//      İşlem başarılı ise 0, değil ise -1 döner
//  ----------------------------------------------------
int resim_al(image *resim,char *resim_dosyasi);

//  ---------------------------------------------------
//  Resim boyutlarını değiştirme
//  resim = boyutlandırma yapılacak resmin  değişkeni 
//  boyutlandirilmis_resim = boyutlandırılmış resmin image türünden adresi
//  genilsik = boyutlandırılacak genişlik
//  yukseklik = boyutlandırılacak yükseklik
//  Dönen değer;
//      İşlem başarılı ise 0, değil ise -1 döner
//  ----------------------------------------------------
int resim_boyutlandir(image resim,image *boyutlandirilmis_resim,int genilsik,int yukseklik);

//  ---------------------------------------------------
//  Tespit edilen nesneleri resim üzerinde kare içerisine almaya yarar
//  resim = image tipinde resim değişkeni adresi
//  tespitlerim = tespit edilen nesneleri barındıran tespit_edilenler türünden değişken
//  nesnelerim = ağın eğitildiği nesnelerin  listesi
//  esik_deger = tespit edilen nesneleri filtrelemek için kullanılan eşik değeri
//  Dönen değer;
//      İşlem başarılı ise 0, değil ise -1 döner
//  ----------------------------------------------------
int tespit_edilenleri_resime_ciz(image *resim, tespit_edilenler *tespitlerim, nesneler nesnelerim,image **alphabet, float esik_deger);

//  ---------------------------------------------------
//  resim değişkeninini kaydetmeye yarar.
//  resim = image tipinde resim değişkeni
//  dosya_adi = kaydedilecek dosyanın adresi ve adı
//  Dönen değer;
//      İşlem başarılı ise 0, değil ise -1 döner
//  ----------------------------------------------------
int resim_kaydet(image resim, char *dosya_adi);

//  ---------------------------------------------------
//  Yüklenen ağın bellekten silinmesini sağlar
//  ag = network tipinde ağın adresi
//  Dönen değer;
//      İşlem başarılı ise 0, değil ise -1 döner
//  ----------------------------------------------------
int ag_sil(network *ag);

//  ---------------------------------------------------
//  Yüklenen resim dosyasının bellekten silinmesini sağlar
//  resim = image tipinde resim değişkeni 
//  Dönen değer;
//      İşlem başarılı ise 0, değil ise -1 döner
//  ----------------------------------------------------
int resim_sil(image resim);

//  ---------------------------------------------------
//  tespit edilen nesneleri silme fonksiyonu
//  tespitler = silinecek tespit edilen nesnelerin değişkeni
//  Dönen değer;
//      İşlem başarılı ise 0, değil ise -1 döner
//  ----------------------------------------------------
int tespit_edilenler_sil(tespit_edilenler *tespitler);

//  ---------------------------------------------------
//  Açılan görüntü arayüzünden görüntü almaya yarar
//  arayuz = oluşturulan arayüzün adresi
//  goruntu = alınan görüntünün image tipinde değişkenin adresi
//  goruntu_matris = alınan görüntü matrisinin mat_cv tipinde değişkenin adresi
//  genislik = alınacak görüntünün genişliği
//  yukseklik = alınacak görüntünün yüksekliği
//  Dönen değer;
//      İşlem başarılı ise 0, değil ise -1 döner
//  ----------------------------------------------------
int goruntu_al(cap_cv *arayuz,image *goruntu,mat_cv **goruntu_matris,int genislikw,int yukseklik);

//  ---------------------------------------------------
//  Görüntü almak için bir arayüz oluşturur
//  arayuz = oluşturulan arayüzün adresi
//  adres = açılacak olan arayüzün dosya adresi veya url adresi
//  Dönen değer;
//      İşlem başarılı ise 0, değil ise -1 döner
//  ----------------------------------------------------
int goruntu_ac(cap_cv **arayuz,char *adres);

//  ---------------------------------------------------
//  Görüntü almak için bir arayüz oluşturur
//  arayuz = oluşturulan arayüzün adresi
//  Dönen değer;
//      Açılan arayüzdeki görüntünün FPS değeri
//  ----------------------------------------------------
int goruntu_fps(cap_cv *arayuz);

//  ---------------------------------------------------
//  Görüntü bir pencere açarak ekrana yansıtır
//  resim = görüntünün değişkeni
//  pencere_ismi = Açılan pencerenin ismi
//  Dönen değer;
//      İşlem başarılı ise 0, değil ise -1 döner
//  ----------------------------------------------------
int resim_goster(image resim, char *pencere_ismi);

//  ---------------------------------------------------
//  Tespit edilen nesneleri görüntü matrisi üzerinde kare içerisine alır
//  resim = görüntünün matris değişkeni adresi
//  tespitlerim = tespit edilen nesneler
//  nesnelerim = nesnelerin isim listesini tutan değişken
//  esik_deger = tespit edilen nesneleri filtrelemek için eşik değeri
//  Dönen değer;
//      İşlem başarılı ise 0, değil ise -1 döner
//  ----------------------------------------------------
int tespit_edilenleri_matrise_ciz(mat_cv *resim, tespit_edilenler tespitlerim, nesneler nesnelerim, float esik_deger);

//  ---------------------------------------------------
//  Kameradan görüntü almak için bir arayüz oluşturur
//  arayuz = oluşturulan arayüzün adresi
//  no = Kamera numarası
//  Dönen değer;
//      İşlem başarılı ise 0, değil ise -1 döner
//  ----------------------------------------------------
int kamera_ac(cap_cv **arayuz, int no);



int tespit_edilenler_kopyala(tespit_edilenler kaynak,tespit_edilenler *hedef);


#endif