#include "darknetFO.h"

int nesneler_yukle(nesneler *nesnelerim, char *dosya_adresi)
{
    nesnelerim->liste = get_labels_custom(dosya_adresi, &nesnelerim->adet);
    return 0;
}

int ag_yukle(network **ag, char *ayar_dosyasi, char *agirlik_dosyasi)
{
    *ag = load_network(ayar_dosyasi, agirlik_dosyasi, 0);
    return 0;
}

int resim_al(image *resim, char *resim_dosyasi)
{
    *resim = load_image_stb(resim_dosyasi, 3);
    return 0;
}

int resim_boyutlandir(image resim, image *boyutlandirilmis_resim, int genilsik, int yukseklik)
{
    *boyutlandirilmis_resim = resize_image(resim, genilsik, yukseklik);
    return 0;
}

int resim_al_boyutlandir(image *resim, char *resim_dosyasi, int genislik, int yukseklik)
{
    *resim = load_image_color(resim_dosyasi, genislik, yukseklik);
    return 0;
}

int ag_calistir(network *ag, image resim, float esik_deger, tespit_edilenler *tespitlerim)
{

    int adet = 0;

    network_predict_image(ag, resim);

    printf("Nesneler alınıyor...\n");
    // Tespit edilen nesneler alınıyor
    tespitlerim->liste = get_network_boxes(ag, ag->w, ag->h, esik_deger, 0.5, 0, 1, &adet, 0);
    tespitlerim->tespit_adet = adet;

    printf("Filtreleniyor...\n");

    // Nesneler sıralanıyor
    do_nms_sort(tespitlerim->liste, adet, tespitlerim->liste->classes, esik_deger);

    printf("Listeleniyor...\n");

    tespitlerim->nesne_listesi = (int *)calloc(sizeof(int), adet);

    for (int i = 0; i < adet; i++)
    {
        float en_yuksek = 0;
        tespitlerim->nesne_listesi[i] = -1;

        for (int j = 0; j < tespitlerim->liste->classes; j++)
        {
            detection *tespit_edilen = tespitlerim->liste + i;
            int *nesne_no = tespitlerim->nesne_listesi + i;

            if (tespit_edilen->prob[j] < esik_deger)
                continue;

            if (en_yuksek < tespit_edilen->prob[j])
            {
                *nesne_no = j;
                en_yuksek = tespit_edilen->prob[j];
            }
        }
        printf("%d=%d=%f\n", i, tespitlerim->nesne_listesi[i], en_yuksek);
    }

    tespitlerim->nesne_adet = 0;
    for (int i = 0; i < adet; i++)
        if (tespitlerim->nesne_listesi[i] > -1)
            tespitlerim->nesne_adet++;

    return 0;
}

int tespit_edilenleri_resime_ciz(image *resim, tespit_edilenler *tespitlerim, nesneler nesnelerim, image **alphabet, float esik_deger)
{

    if (!&tespitlerim || tespitlerim->nesne_adet == 0)
        return -1;

    for (int i = 0; i < tespitlerim->tespit_adet; i++)
    {

        detection *tespit_edilen = tespitlerim->liste + i;
        int *nesne_numarasi = tespitlerim->nesne_listesi + i;


        if (*nesne_numarasi == -1 || !tespit_edilen)
            continue;

        if (alphabet)
        {
            float rgb[3];

            rgb[0] = 1;
            rgb[1] = 0;
            rgb[2] = 0;

            int left = (tespit_edilen->bbox.x - tespit_edilen->bbox.w / 2.) * resim->w;
            int right = (tespit_edilen->bbox.x + tespit_edilen->bbox.w / 2.) * resim->w;
            int top = (tespit_edilen->bbox.y - tespit_edilen->bbox.h / 2.) * resim->h;
            int bot = (tespit_edilen->bbox.y + tespit_edilen->bbox.h / 2.) * resim->h;

            image label = get_label(alphabet, nesnelerim.liste[*nesne_numarasi], 1);
            //printf("%f %d \n",top + tespit_edilen->bbox.w,left);
            draw_label(*resim, top + tespit_edilen->bbox.w, left, label, rgb);
            //printf("----------\n");
        }
        else
        {
            printf("Alfabe yok\n");
        }
        draw_bbox(*resim, tespit_edilen->bbox, 5, 1, 0, 0);
    }

    return 0;
}

int tespit_edilenleri_matrise_ciz(mat_cv *resim, tespit_edilenler tespitlerim, nesneler nesnelerim, float esik_deger)
{

    /*for (int i = 0; i < tespitlerim.adet; i++)
    {
        detection *tespit_edilen = tespitlerim.liste + i;
        int nesne_numarasi = tespitlerim.nesne_listesi[i];*/
    image **alphabet = load_alphabet();

    draw_detections_cv_v3(resim, tespitlerim.liste, tespitlerim.tespit_adet, esik_deger, nesnelerim.liste, alphabet, tespitlerim.liste->classes, 0);

    /* image label = get_label(alphabet, nesnelerim.liste[nesne_numarasi], (resim->h * .03) / 10);
            draw_label(*resim, top + tespit_edilen->bbox.w, left, label, rgb);*/

    // draw_bbox(*resim, tespit_edilen->bbox, 5, 1, 0, 0);
    // }
}

int resim_kaydet(image resim, char *dosya_adi)
{
    save_image_png(resim, dosya_adi);
    return 0;
}

int ag_sil(network *ag)
{
    free_network(*ag);
    return 0;
}

int resim_sil(image resim)
{
    free_image(resim);
    return 0;
}

int goruntu_al(cap_cv *kamera, image *goruntu, mat_cv **goruntu_matris, int w, int h)
{
    if (goruntu_matris == NULL)
    {
        mat_cv **temp;
        *goruntu = get_image_from_stream_letterbox(kamera, w, h, 3, temp, 0);
        release_mat(temp);
    }
    else
    {
        *goruntu = get_image_from_stream_letterbox(kamera, w, h, 3, goruntu_matris, 0);
    }

    return 0;
}

int goruntu_ac(cap_cv **kamera, char *adres)
{
    *kamera = get_capture_video_stream(adres);
    if (!*kamera)
        return -1;

    return 0;
}

int kamera_ac(cap_cv **kamera, int no)
{
    *kamera = get_capture_webcam(no);
    if (!*kamera)
        return -1;

    return 0;
}

int goruntu_fps(cap_cv *kamera)
{
    return get_stream_fps_cpp_cv(kamera);
}

int tespit_edilenler_sil(tespit_edilenler *tespitler)
{

    free_detections(tespitler->liste, tespitler->tespit_adet);
    free(tespitler->nesne_listesi);

    return 0;
}

int tespit_edilenler_kopyala(tespit_edilenler kaynak,tespit_edilenler *hedef)
{
    if(kaynak.tespit_adet < 1) return -1;

    hedef->liste = (detection*) calloc(sizeof(detection),kaynak.tespit_adet);
    hedef->nesne_listesi = (int*) calloc(sizeof(int),kaynak.tespit_adet);

    memcpy(hedef->liste, kaynak.liste ,sizeof(detection) * kaynak.tespit_adet);
    memcpy(hedef->nesne_listesi, kaynak.nesne_listesi ,sizeof(int) * kaynak.tespit_adet);

    hedef->tespit_adet = kaynak.tespit_adet;
    hedef->nesne_adet = kaynak.nesne_adet;

    return 0;
}

int resim_goster(image resim, char *pencere_ismi)
{
    show_image_cv(resim, pencere_ismi);
    if (wait_key_cv(5) >= 0)
        exit(0);
}