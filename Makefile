CC = gcc


TARGET_LIB_PATH = /usr/local/lib/darknet/
TARGET_INC_PATH = /usr/local/include/darknet/
TARGET_BIN_PATH = /usr/local/bin/


ROOT_PATH = $(PWD)/
BUILD_PATH = $(ROOT_PATH)/
SRC_PATH = $(ROOT_PATH)/src/
LIB_PATH = $(ROOT_PATH)/libs/


IMG_SOURCE = $(SRC_PATH)darknetFO.c $(SRC_PATH)resim.c 
IMG_TARGET = FO_resim
IMG_FLAGS = -L$(LIB_PATH) -ldarknet -I$(ROOT_PATH)


VIDEO_SOURCE = $(SRC_PATH)darknetFO.c $(SRC_PATH)video.c 
VIDEO_TARGET = FO_video
VIDEO_FLAGS =  -L$(LIB_PATH) -ldarknet -I$(ROOT_PATH) 


CAM_SOURCE = $(SRC_PATH)darknetFO.c $(SRC_PATH)kamera.c 
CAM_TARGET = FO_kamera
CAM_FLAGS =  -L$(LIB_PATH) -ldarknet -pthread -I$(ROOT_PATH)



all:
	$(CC) -g $(IMG_FLAGS)  $(IMG_SOURCE) -o $(BUILD_PATH)/$(IMG_TARGET)
	$(CC) -g $(VIDEO_FLAGS)  $(VIDEO_SOURCE) -o $(BUILD_PATH)/$(VIDEO_TARGET)
	$(CC) -g $(CAM_FLAGS)  $(CAM_SOURCE) -o $(BUILD_PATH)/$(CAM_TARGET)

install : 
	cp  $(BUILD_PATH)/$(IMG_TARGET) $(TARGET_BIN_PATH)
	cp $(BUILD_PATH)/$(VIDEO_TARGET) $(TARGET_BIN_PATH)
	mkdir $(TARGET_LIB_PATH)
	mkdir $(TARGET_INC_PATH)
	cp ./libs/libdarknet.so $(TARGET_LIB_PATH)
	cp ./include/darknet.h	$(TARGET_INC_PATH)

resim:
	LD_LIBRARY_PATH=$(LIB_PATH) $(BUILD_PATH)/$(IMG_TARGET)

video:
	LD_LIBRARY_PATH=$(LIB_PATH) $(BUILD_PATH)/$(VIDEO_TARGET)

kamera:
	LD_LIBRARY_PATH=$(LIB_PATH) $(BUILD_PATH)/$(CAM_TARGET)

clean:
	rm -f output/*
	rm -rf $(TARGET_LIB_PATH)
	rm -rf $(TARGET_INC_PATH)
	rm -f $(TARGET_BIN_PATH)/$(IMG_TARGET)
	rm -f $(TARGET_BIN_PATH)/$(VIDEO_TARGET)
